import moviepy.video.fx.all as vfx
import numpy as np
from moviepy.editor import VideoFileClip
from typedconfig import Config, group_key, key, section
from typedconfig.source import EnvironmentConfigSource, IniFileConfigSource
from typing import List


# MoviePy Docu: https://zulko.github.io/moviepy/index.html
# hints:
# https://medium.com/@TejasBob/moviepy-is-awesome-part1-f90e91fffbb9
# https://medium.com/@TejasBob/moviepy-is-awesome-part2-73b04e2338b0


class Options:
    preview: bool = False


options = Options()


def split_str(s: str) -> List[str]:
    """Split comma separated string into list for usage by 'typed_config'"""
    return [x.strip() for x in s.split(",")]


@section("input")
class InputConfig(Config):
    file = key(cast=str)
    start = key(cast=float)
    end = key(cast=float)


@section("effect")
class EffectConfig(Config):
    name = key(cast=str)
    duration = key(cast=float)
    nframes = key(cast=int, required=False, default=0)


@section("output")
class OutputConfig(Config):
    snapshots = key(cast=int)
    basename = key(cast=str)


class AppConfig(Config):
    input = group_key(InputConfig)
    effect = group_key(EffectConfig)
    output = group_key(OutputConfig)


def sumup(clip, d, nframes):
    """Replaces each frame at time t by the sum of `nframes` equally spaced frames
    taken in the interval [t-d, t+d]."""

    def fl(gf, t):
        tt = np.linspace(t - d, t + d, nframes)
        sum = np.sum(1.0 * np.array([gf(t_) for t_ in tt], dtype="uint16"), axis=0)
        clipped = np.clip(sum, 0, 255)
        return clipped.astype("uint8")

    return clip.fl(fl)


def maxval(clip, d, nframes):
    """Replaces each frame at time t by the sum of `nframes` equally spaced frames
    taken in the interval [t-d, t+d]."""

    def fl(gf, t):
        tt = np.linspace(t - d, t + d, nframes)
        sum = np.max(1.0 * np.array([gf(t_) for t_ in tt], dtype="uint16"), axis=0)
        clipped = np.clip(sum, 0, 255)
        return clipped.astype("uint8")

    return clip.fl(fl)


def snapshot(clip, time, effect, duration, nframes=None):
    if not (nframes):
        nframes = int(duration * 2 * Input.fps + 1)
    clip.fx(effect, d=duration, nframes=nframes).save_frame(f"snap-{time}.png", t=time)


def prepare_input(
    filename: str, start_time: float, end_time: float, scale_down=False
) -> VideoFileClip:
    Input = VideoFileClip(filename).subclip(start_time, end_time)
    if scale_down:
        Input = Input.resize(0.125)
    return Input


def append_effect(
    clip: VideoFileClip, effect_name: str, duration: float, nframes: int = None
) -> VideoFileClip:
    if not nframes:
        nframes = int(duration * 2 * Input.fps + 1)

    dispatch = {
        "maxval": maxval,
        "sumup": sumup,
        "supersample": vfx.supersample,
    }

    # Output=clip.fx(vfx.supersample, d=duration, nframes=nframes)
    # Output=clip.fx(sumup, d=duration, nframes=nframes)
    return clip.fx(dispatch[effect_name], d=duration, nframes=nframes)


def render_snapshots(clip, basename: str, numsnapshots):
    for snaptime in map(
        int,
        np.linspace(10, config.input.end - config.input.start - 10, numsnapshots),
    ):
        print(f"snapshotting at {snaptime}")
        Output.save_frame(f"{basename}_{snaptime}.png", t=snaptime)


def render_movie(clip: VideoFileClip, basename):
    clip.write_videofile(
        f"{basename}.mp4", preset="ultrafast", codec="mpeg4", audio=False
    )


config = AppConfig()
config.add_source(EnvironmentConfigSource())
config.add_source(IniFileConfigSource("my.cfg"))
config.read()

Input = prepare_input(
    filename=config.input.file,
    start_time=config.input.start,
    end_time=config.input.end,
    scale_down=options.preview,
)
Output = append_effect(
    clip=Input,
    effect_name=config.effect.name,
    duration=config.effect.duration,
    nframes=config.effect.nframes,
)
basename = f"{config.output.basename}_{config.effect.name}_{config.effect.duration}_{config.effect.nframes}"
# render_snapshots(
#     Output,
#     basename=basename,
#     numsnapshots=config.output.snapshots,
# )
# render_movie(Output, basename=basename)

Short = Output.subclip(83,93) 
Loop = vfx.make_loopable(Short, 5)
Loop.write_videofile("Loop.mp4", preset="ultrafast", codec="mpeg4", audio=False)
Short.write_videofile("Short.mp4", preset="ultrafast", codec="mpeg4", audio=False)

