Lighttrail
==========

Rendering alternatives:
-----------------------

Dancing dots: Start with `n` individual frames with time delay `dT`
Fade-in of lighttrail


Rendering parameters
--------------------

* Start time (time when actual video content starts, e.g ignition of candles)
* number of samples for dancing lights
* Start of lighttrail
* lighttrail fade-in duration
* lighttrail algorithm
  * moving

Flow
----

* Input
  * Video file
* 